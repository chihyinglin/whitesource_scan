FROM openjdk:8-jre-slim

 
RUN apt update && apt install -y wget && apt-get clean
ENV API_KEY='' PRODUCT_TOKEN='' PROJECT_NAME='' SCAN_PATH=SourceCode LOG_UID=0 LOG_GID=0
WORKDIR /work

# copy files
COPY ./linux .
RUN chmod a+x scan.sh

# download latest unified agent
ARG LATEST_UNIFIED_AGENT_VERSION
RUN wget --progress=bar:force https://github.com/whitesource/unified-agent-distribution/releases/download/${LATEST_UNIFIED_AGENT_VERSION}/wss-unified-agent.jar 

CMD bash -c 'source ./scan.sh'
