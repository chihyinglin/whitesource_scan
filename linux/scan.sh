#!/bin/bash

# exit immediately if a command exits with a non-zero status.
set -e

export

# ==================
# env variables:
#   API_KEY
#   PRODUCT_TOKEN
#   PROJECT_NAME
#   LOG_UID
#   LOG_GID
#   CHECK_POLICY
# ==================

# set check_policy
if [ x"${CHECK_POLICY}" != x"" ]
then
  sed -i 's/checkPolicies=.*$/checkPolicies=true/g' ./wss-unified-agent.config
else
  sed -i 's/checkPolicies=.*$/checkPolicies=false/g' ./wss-unified-agent.config
fi

# scan
java -Dfile.encoding=UTF-8 -jar ./wss-unified-agent.jar \
	-c ./wss-unified-agent.config \
	-d ${SCAN_PATH} \
	-apiKey ${API_KEY} \
	-productToken ${PRODUCT_TOKEN} \
	-project ${PROJECT_NAME}

# modify the permission of logs
if [ ! -z ${LOG_UID} ] && [ ! -z ${LOG_GID} ]; then
  echo Update log permissions...
  chown -R ${LOG_UID}:${LOG_GID} whitesource
fi
