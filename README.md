# WhiteSource_scan

## Intruduction
Use SCA tool "WhiteSource" to scan files

#### get basic info:
- Two import parameter from https://saas.whitesourcesoftware.com/Wss/WSS.html#!adminOrganization_integration
  - api key
  - product token

# usage
## parameters
- Environment variables
  - **API_KEY**
  - **PRODUCT_TOKEN**
  - **PROJECT_NAME**
  - (optional) **LOG_UID** / **LOG_GID**
    - modify the UID/GID of logs 
  - (optional) **CHECK_POLICY**
    - fail the scan if policy check fail
## linux shell
- setup environment
  1. install JAVA
      - `apt install openjdk-8-jre`
  1. download latest unified-agent tool
      - `cd linux && wget https://unified-agent.s3.amazonaws.com/wss-unified-agent.jar`
        - or download a specific version from https://github.com/whitesource/unified-agent-distribution/releases/
- example:
  - ```
    cd linux
    
    API_KEY=xxx \
    PRODUCT_TOKEN=xxx \
    SCAN_PATH=SourceCode \
    PROJECT_NAME=test_linux \
    source ./scan.sh
    ```
## linux docker
- image
  - `registry.gitlab.com/chihyinglin/whitesource_scan`
  - tags
    - `latest`: the latest build
    - `v*.*.*`: image with same version unified agent(https://github.com/whitesource/unified-agent-distribution/releases/) 
- Volumes
  - mount the source code directory to `/work/SourceCode`
  - (Optional) mount the log directory to `/work/SourceCode`
- Example:
  - ```
    docker run -it \
      -v `pwd`/src:/work/SourceCode \
      -v `pwd`/log:/work/whitesource \
      -e API_KEY=xxx \
      -e PRODUCT_TOKEN=xxx \
      -e PROJECT_NAME=test_docker \
      -e LOG_UID=`id -u` \
      -e LOG_GID=`id -g` \
      -e CHECK_POLICY=false \
      registry.gitlab.com/chihyinglin/whitesource_scan:latest
    ```
